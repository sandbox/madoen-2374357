<?php

/**
 * @file
 * Form to configure Image Storage Account.
 */

/**
 * Callback for the admin configuration page.
 */
function eifs_admin($form, &$form_state) {
  // This is to specify the vertical tab holder.
  $form['admin_form_tab'] = array(
    '#type' => 'vertical_tabs',
  );
  // Picasa account credential
  $form['picasa'] = array(
    '#title' => t('Picasa'),
    '#type' => 'fieldset',
    '#description' => t('Picasa account credential infos.'),
    '#group' => 'admin_form_tab',
  );

  $form['picasa']['eifs_google_account'] = array(
    '#title' => t('Google mail account'),
    '#type' => 'textfield',
    '#description' => t('Please input your Google mail account.'),
    '#default_value' => variable_get('eifs_google_account'),
  );

  $form['picasa']['eifs_google_reset'] = array(
    '#type' => 'checkbox',
    '#title' => t('Change the password.'),
    '#default_value' => empty(variable_get('eifs_google_pass')),
    '#disabled' => empty(variable_get('eifs_google_pass')),
    '#ajax' => array(
      'callback' => 'eifs_google_pass_callback',
      'wrapper' => 'eifs_google_pass',
      'effect' => 'fade',
    ),
  );

  $form['picasa']['eifs_google_pass'] = array(
    '#title' => t('Google mail password'),
    '#type' => 'password',
    '#prefix' => '<div id="eifs_google_pass">',
    '#suffix' => '</div>',
    '#description' => t('Please input your Google mail password.'),
  );

  if (empty($form_state['values']['eifs_google_reset']) &&
    !empty(variable_get('eifs_google_pass'))) {
    $form['picasa']['eifs_google_pass']['#disabled'] = TRUE;
  }

  // Imageshack account credential
  $form['imageshack'] = array(
    '#title' => t('Imageshack'),
    '#type' => 'fieldset',
    '#description' => t('Imageshack account credential infos.'),
    '#group' => 'admin_form_tab',
  );

  $form['imageshack']['eifs_imageshack_api'] = array(
    '#title' => t('Imageshack API'),
    '#type' => 'textfield',
    '#description' => t('Please input your Imageshack API code.'),
    '#default_value' => variable_get('eifs_imageshack_api'),
  );

  $form['imageshack']['eifs_imageshack_account'] = array(
    '#title' => t('Imageshack account'),
    '#type' => 'textfield',
    '#description' => t('Please input your Imageshack account.'),
    '#default_value' => variable_get('eifs_imageshack_account'),
  );

  $form['imageshack']['eifs_imageshack_reset'] = array(
    '#type' => 'checkbox',
    '#title' => t('Change the password.'),
    '#default_value' => empty(variable_get('eifs_imageshack_pass')),
    '#disabled' => empty(variable_get('eifs_imageshack_pass')),
    '#ajax' => array(
      'callback' => 'eifs_imageshack_pass_callback',
      'wrapper' => 'eifs_imageshack_pass',
      'effect' => 'fade',
    ),
  );

  $form['imageshack']['eifs_imageshack_pass'] = array(
    '#title' => t('Imageshack password'),
    '#type' => 'password',
    '#prefix' => '<div id="eifs_imageshack_pass">',
    '#suffix' => '</div>',
    '#description' => t('Please input your Imageshack mail password.'),
  );

  if (empty($form_state['values']['eifs_imageshack_reset']) &&
    !empty(variable_get('eifs_imageshack_pass'))) {
    $form['imageshack']['eifs_imageshack_pass']['#disabled'] = TRUE;
  }

  // Imgur account credential
  $form['imgur'] = array(
    '#title' => t('Imgur'),
    '#type' => 'fieldset',
    '#description' => t('Imgur account credential infos.'),
    '#group' => 'admin_form_tab',
  );

  $form['imgur']['eifs_imgur_id'] = array(
    '#title' => t('Imgur client ID'),
    '#type' => 'textfield',
    '#description' => t('Please input your Imgur client ID.'),
    '#default_value' => variable_get('eifs_imgur_id'),
  );

  $form['imgur']['eifs_imgur_secret'] = array(
    '#title' => t('Imgur client secret'),
    '#type' => 'textfield',
    '#description' => t('Please input your Imgur client secret.'),
    '#default_value' => variable_get('eifs_imgur_secret'),
  );

  $form['imgur']['eifs_imgur_account'] = array(
    '#title' => t('Imgur account'),
    '#type' => 'textfield',
    '#description' => t('Please input your Imgur account.'),
    '#default_value' => variable_get('eifs_imgur_account'),
  );

  $form['imgur']['eifs_imgur_reset'] = array(
    '#type' => 'checkbox',
    '#title' => t('Change the password.'),
    '#default_value' => empty(variable_get('eifs_imgur_pass')),
    '#disabled' => empty(variable_get('eifs_imgur_pass')),
    '#ajax' => array(
      'callback' => 'eifs_imgur_pass_callback',
      'wrapper' => 'eifs_imgur_pass',
      'effect' => 'fade',
    ),
  );

  $form['imgur']['eifs_imgur_pass'] = array(
    '#title' => t('Imgur password'),
    '#type' => 'password',
    '#prefix' => '<div id="eifs_imgur_pass">',
    '#suffix' => '</div>',
    '#description' => t('Please input your Imgur mail password.'),
  );

  if (empty($form_state['values']['eifs_imgur_reset']) &&
    !empty(variable_get('eifs_imgur_pass'))) {
    $form['imgur']['eifs_imgur_pass']['#disabled'] = TRUE;
  }

  // Postimage account credential
  $form['postimage'] = array(
    '#title' => t('Postimage'),
    '#type' => 'fieldset',
    '#description' => t('Postimage account credential infos.'),
    '#group' => 'admin_form_tab',
  );

  $form['postimage']['eifs_postimage_account'] = array(
    '#title' => t('Postimage account'),
    '#type' => 'textfield',
    '#description' => t('Please input your Postimage account.'),
    '#default_value' => variable_get('eifs_postimage_account'),
  );

  $form['postimage']['eifs_postimage_reset'] = array(
    '#type' => 'checkbox',
    '#title' => t('Change the password.'),
    '#default_value' => empty(variable_get('eifs_postimage_pass')),
    '#disabled' => empty(variable_get('eifs_postimage_pass')),
    '#ajax' => array(
      'callback' => 'eifs_postimage_pass_callback',
      'wrapper' => 'eifs_postimage_pass',
      'effect' => 'fade',
    ),
  );

  $form['postimage']['eifs_postimage_pass'] = array(
    '#title' => t('Postimage password'),
    '#type' => 'password',
    '#prefix' => '<div id="eifs_postimage_pass">',
    '#suffix' => '</div>',
    '#description' => t('Please input your Postimage mail password.'),
  );

  if (empty($form_state['values']['eifs_postimage_reset']) &&
    !empty(variable_get('eifs_postimage_pass'))) {
    $form['postimage']['eifs_postimage_pass']['#disabled'] = TRUE;
  }

  return system_settings_form($form);
}

/**
 * Callback for eifs_google_reset.
 */
function eifs_google_pass_callback($form, $form_state) {
  return $form['picasa']['eifs_google_pass'];
}

/**
 * Callback for eifs_imageshack_reset.
 */
function eifs_imageshack_pass_callback($form, $form_state) {
  return $form['imageshack']['eifs_imageshack_pass'];
}

/**
 * Callback for eifs_imgur_reset.
 */
function eifs_imgur_pass_callback($form, $form_state) {
  return $form['imgur']['eifs_imgur_pass'];
}

/**
 * Callback for eifs_postimage_reset.
 */
function eifs_postimage_pass_callback($form, $form_state) {
  return $form['postimage']['eifs_postimage_pass'];
}

/**
 * Validation step for eifs_admin form.
 */
function eifs_admin_validate($form, &$form_state) {
  // Validation for Picasa account credential
  if(!empty($form_state['values']['eifs_google_account']) xor
    !empty($form_state['values']['eifs_google_pass'])) {
    if(empty($form_state['values']['eifs_google_account'])) {
      form_set_error('eifs_google_account',
        t('Please complete your Picasa account credential infos.'));
    }
    if(!empty($form_state['values']['eifs_google_reset'])) {
      if(empty($form_state['values']['eifs_google_pass'])) {
        form_set_error('eifs_google_pass',
          t('Please complete your Picasa account credential infos.'));
      }
    }
    else {
      $form_state['values']['eifs_google_pass'] = variable_get('eifs_google_pass');
    }
  }

  // Validation for Imageshack account credential
  if (in_array(
    TRUE,[
    empty($form_state['values']['eifs_imageshack_api']),
    empty($form_state['values']['eifs_imageshack_account']),
    empty($form_state['values']['eifs_imageshack_pass'])
    ]) AND
    !(empty($form_state['values']['eifs_imageshack_api']) AND
    empty($form_state['values']['eifs_imageshack_account']) AND
    empty($form_state['values']['eifs_imageshack_pass'])
    )) {
    if(empty($form_state['values']['eifs_imageshack_api'])) {
      form_set_error('eifs_imageshack_api',
        t('Please complete your Imageshack account credential infos.'));
    }
    if(empty($form_state['values']['eifs_imageshack_account'])) {
      form_set_error('eifs_imageshack_account',
        t('Please complete your Imageshack account credential infos.'));
    }
    if(!empty($form_state['values']['eifs_imageshack_reset'])) {
      if(empty($form_state['values']['eifs_imageshack_pass'])) {
        form_set_error('eifs_imageshack_pass',
          t('Please complete your Imageshack account credential infos.'));
      }
    }
    else {
      $form_state['values']['eifs_imageshack_pass'] = variable_get('eifs_imageshack_pass');
    }
  }

  // Validation for Imgur account credential
  if (in_array(
    TRUE,[
    empty($form_state['values']['eifs_imgur_id']),
    empty($form_state['values']['eifs_imgur_secret']),
    empty($form_state['values']['eifs_imgur_account']),
    empty($form_state['values']['eifs_imgur_pass'])
    ]) AND
    !(empty($form_state['values']['eifs_imgur_id']) AND
    empty($form_state['values']['eifs_imgur_secret']) AND
    empty($form_state['values']['eifs_imgur_account']) AND
    empty($form_state['values']['eifs_imgur_pass'])
    )) {
    if(empty($form_state['values']['eifs_imgur_id'])) {
      form_set_error('eifs_imgur_id',
        t('Please complete your Imgur account credential infos.'));
    }
    if(empty($form_state['values']['eifs_imgur_secret'])) {
      form_set_error('eifs_imgur_secret',
        t('Please complete your Imgur account credential infos.'));
    }
    if(empty($form_state['values']['eifs_imgur_account'])) {
      form_set_error('eifs_imgur_account',
        t('Please complete your Imgur account credential infos.'));
    }
    if(!empty($form_state['values']['eifs_imgur_reset'])) {
      if(empty($form_state['values']['eifs_imgur_pass'])) {
        form_set_error('eifs_imgur_pass',
          t('Please complete your Imgur account credential infos.'));
      }
    }
    else {
      $form_state['values']['eifs_imgur_pass'] = variable_get('eifs_imgur_pass');
    }
  }

  // Validation for Postimage account credential
  if(!empty($form_state['values']['eifs_postimage_account']) xor
    !empty($form_state['values']['eifs_postimage_pass'])) {
    if(empty($form_state['values']['eifs_postimage_account'])) {
      form_set_error('eifs_postimage_account',
        t('Please complete your Postimage account credential infos.'));
    }
    if(!empty($form_state['values']['eifs_postimage_reset'])) {
      if(empty($form_state['values']['eifs_postimage_pass'])) {
        form_set_error('eifs_postimage_pass',
          t('Please complete your Postimage account credential infos.'));
      }
    }
    else {
      $form_state['values']['eifs_postimage_pass'] = variable_get('eifs_postimage_pass');
    }
  }
}
